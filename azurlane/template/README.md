# Template usage
Templates are made so you don't have to write any HTML and JS, and only do little CSS adjustments and fill in a JSON file with values.

# Base files
The [template HTML file](https://bitbucket.org/Rhiki/rhiki.bitbucket.io/src/master/azurlane/template/html-template.html) uses [BaseJS.js](https://bitbucket.org/Rhiki/rhiki.bitbucket.io/src/master/azurlane/BaseJS.js) and [BaseCSS.css](https://bitbucket.org/Rhiki/rhiki.bitbucket.io/src/master/azurlane/BaseCSS.css) to fill in all the needed JS and style sheets.

# html-template.html:
The only JS you need to write is to fill in the URL of the accompanying JSON in the *script* tag at the bottom of the *body* tag:
```javascript
/*Example:*/ BaseJS.initializePage("./a-foreign-idol.json");
```

You need to fill in the following CSS rules:

*For infos on linear-gradient check out: [W3schools](https://www.w3schools.com/css/css3_gradients.asp) or [MDN web docs](https://developer.mozilla.org/en-US/docs/Web/CSS/linear-gradient).*

*For infos on media queries check out: [W3schools 1](https://www.w3schools.com/css/css_rwd_mediaqueries.asp), [W3schools 2](https://www.w3schools.com/cssref/css3_pr_mediaquery.asp) or [MDN web docs](https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries).*

\#contentContainer:
Set the background to any color your want and/or use linear-gradient to set a gradient effect.
```css
#contentContainer{
    background-image: /*your color/gradient here*/;
    /*Example:*/ background-image: linear-gradient(to right, #fbf9f8 30%, #ca4250 95%);
}
```

.emptyCell: Similar to #contentContainer, set the background color for the leading empty cell on the left.
```css
.emptyCell{
    background-image: /*your color/gradient here*/;
    /*Example:*/ background-image: linear-gradient(to right, #fbf9f8 0%, #ca4250 99%);
}
```

.border: Set the color of the border.
```css
.border{
    /*Example:*/ border-color: #a30011;
}
```
.currency::before: Set the URL of the image that should be shown as currency icon before each currency value and its size.
```css
.currency::before{
    background: url(/*your URL here*/) no-repeat;
    background-size: /*your size here, usually 17-20px*/;
}
```
\#headerimg: (Optional) Move the header image left or right. (negative values move it leftwards, positive values move it rightwards)
```css
#headerimg{
    /*Example:*/ left: -50px;
}
```
::-webkit-scrollbar-track: (Optional, Webkit/Chrome only): Set the color of the scrollbar track.
```css
::-webkit-scrollbar-track {
    /*Example:*/ background: #ca4250;
}
```
::-webkit-scrollbar-thumb: (Optional, Webkit/Chrome only): Set the color of the scrollbar thumb.
```css
::-webkit-scrollbar-thumb {
    /*Example:*/ background: #dd8890;
}
```
::-webkit-scrollbar-humb:hover: (Optional, Webkit/Chrome only): Set the color of the scrollbar thumb, when hovering over it.
```css
::-webkit-scrollbar-thumb:hover {
    /*Example:*/ background: #fff4f2;
}
```
### Media query for low-width windows/screens.
To move the left-hand side image to the header, when there is not enough space for it, we have to set this media query. These styles are only applied when the width of the window is less than the specified max-width. Set the max-width to the amount of pixel, where the image starts overlapping table text to move it to the header section. To better place the image you can use **#headerimage** to set it's *top* and *left* offset.
```css
@media only screen and (max-width: 1000px){ /* Set width here */
    #header{
        overflow: hidden;
    }
    #headerimg{
        position:relative;
        top: -125px; /*Adjust here to change the vertical position of the header image*/
        left: 0px; /*Adjust here to change the horizontal position of the header image*/
    }
    
    .fitToContentIfWide{
        width: 50%;
    }
    
    .emptyCell{
        display: none;
    }
}
```

# json-template.json
```json
{
    "title": "", //Title to be used in in the header section, as well as for the browser tab
    "favIconSrc":"", //URL of the image that should be used as browser tab icon
    "eventDuration": "", //Event duration as text, to be used in the header section
    "headerImg":{ //Info for the image that is used in the header section
        "src":"", //URL of the image to be used
    },
    "storageKey":"", //The key to be used for persisting progess/settings in the browser
    "jsonExportFileName":"", //The prefix to be used for the filename when clicking export
    "shop":[ //Array to store all items the shop sells
        {
            "name": "", //Name of the item
            "imgSrc":"", //URL to the icon of the item
            "rarity":"", //Item rarity (used for coloring the table row); possible values: "C", "R", "E", "SR", "UR"
            "availableAmount":0, //How many of the items are available to buy
            "price": 0 //The price for each item
        }
    ]
}
```