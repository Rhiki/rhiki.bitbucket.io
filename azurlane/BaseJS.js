var BaseJS = (function() {
	
	var publicApi = {};
	
	'use strict';
	
	publicApi.importTableItems = function(url, callback){
		var xhr = new XMLHttpRequest();
		xhr.responseType = "text";
		xhr.onreadystatechange = function() {
			if (xhr.readyState === 4) {
				if (xhr.status === 200) {
					var data = JSON.parse(xhr.responseText);
					if (callback) callback(data);
				}
			}
		};
		xhr.open("GET", url);
		xhr.send();
	};

	/**
	Load a dictionary of persisted settings from localStorage, if available.
	@param storageKey The key to use for retrieving the dict from localStorage.
	@returns The dictionary from the storage; empty dictionary if the entry at storageKey does not exist or localStorage is not available
	*/
	publicApi.loadPersistedStorage = function(storageKey){
		if (typeof localStorage !== 'undefined'){
			var string = localStorage.getItem(storageKey);
			if(string != null) return JSON.parse(string);
		}
		return {};
	};


	/**
	Save the passed dictionary to the localStorage, if it's available.
	@param dictionary The dictionary to save.
	@param key The key it should be stored under.
	*/
	publicApi.saveToLocalStorage = function(dictionary, storageKey){
		if(typeof localStorage !== 'undefined'){
			localStorage.setItem(storageKey, JSON.stringify(dictionary));
		}
	};

	/**
	Export the specified dictionary as JSON to the local filesystem with the specified filename.
	@param dictionary The dictionary to save to the filesystem.
	@param fileName the file name to be used, without file extension.
	*/
	publicApi.exportAsJson = function(dictionary, fileName){
		var a = document.createElement("a");
		var file = new Blob([JSON.stringify(dictionary, null, "\t")], {type: "application/json"});
		a.href = URL.createObjectURL(file);
		a.download = fileName + ".json";
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	};

	/**
	TODO
	*/
	publicApi.importJson = function(event){
		var file = event.target.files[0];
		if(file.type === "application/json"){
			console.log("Importing from file: " + file);
			var reader = new FileReader();
			reader.onload = (function(f) {
				return function(e) {
					dict = JSON.parse(e.target.result);
					updatePersistedStorage(dict, storageKey);
					applyPersistedValues();
				};
			})(file);
			reader.readAsText(file);
		}else{
			console.log("File is not .json. Skipping import.")
		}
	};
	
	publicApi.setHeaderTitle = function(title){
		document.querySelector("#headerTitle").textContent = title;
	};
	
	publicApi.setEventDuration = function(eventDuration){
		document.querySelector("#eventDuration").textContent = eventDuration;
	};
	
	publicApi.setBuyoutPrice = function(buyoutPrice){
		document.querySelector("#buyoutPrice").textContent = buyoutPrice;
	};
	
	publicApi.setHeaderImg = function(headerImgSrc){
		document.querySelector("#headerimg").src = headerImgSrc;
	};
	
	publicApi.setFavIcon = function(favIconSrc, includeParent){
		setFavIconInternal(favIconSrc, document);
		setFavIconInternal(favIconSrc, parent.document)
	};
	
	function setFavIconInternal(favIconSrc, documentElement){
		var existing = documentElement.head.querySelector("link[rel=icon]");
		if(existing){
			existing.href = favIconSrc;
		}else{
			var link = documentElement.createElement("link");
			link.rel = "icon";
			link.href = favIconSrc;
			documentElement.head.appendChild(link);
		}
	}
	
	function createNameCell(tableRowData){
		var cell = document.createElement("td");
		cell.textContent = tableRowData.name;
		cell.classList.add("itemName", "fitToContentIfWide");
		return cell;
	}
	
	function createIconCell(tableRowData){
		var cell = document.createElement("td");
		var img = document.createElement("img");
		img.src = tableRowData.imgSrc;
		img.classList.add("shopIcon");
		cell.classList.add("fitToContent");
		cell.appendChild(img);
		return cell;
	}
	
	function createPriceCell(tableRowData){
		var cell = document.createElement("td");
		cell.textContent = tableRowData.price;
		cell.classList.add("currency", "fitToContent", "priceEach");
		return cell;
	}
	
	function createAvailableAmountCell(tableRowData){
		var cell = document.createElement("td");
		cell.textContent = tableRowData.availableAmount;
		cell.classList.add("fitToContent");
		return cell;
	}
	
	function createRemainingAmountCell(tableRowData){
		var cell = document.createElement("td");
		cell.classList.add("cellRemaining", "fitToContent");
		cell.textContent = tableRowData.availableAmount;
		return cell;
	}
	
	function createRemainingPriceCell(tableRowData){
		var cell = document.createElement("td");
		cell.textContent = tableRowData.availableAmount * tableRowData.price;
		cell.classList.add("currency", "fitToContentWide", "remainingPrice");
		return cell;
	}
	
	function createButton(innerText, cellRemainingAmount, cellRemainingPrice, tableRowData, getNewValue, validateNewValue){
		var button = document.createElement("td");
		button.textContent = innerText;
		button.classList.add("divButton", "fitToContentWide");
		button.onclick = function(){
			var newValue = getNewValue();
			if(validateNewValue(newValue)){
				cellRemainingAmount.textContent = newValue;
				cellRemainingPrice.textContent = cellRemainingAmount.textContent * tableRowData.price;
				forceRedraw();
				updatePersistedStorage(tableRowData.name, newValue);
			}
		};
		return button;
	}
	
	
	function createButtonMore(tableRowData, cellRemainingAmount, cellRemainingPrice){
		return createButton("+", cellRemainingAmount, cellRemainingPrice, tableRowData,
			function(){ return parseInt(cellRemainingAmount.innerText, 10) + 1; },
			function(newValue){ return newValue <= tableRowData.availableAmount; }
		);
	}
	
	function createButtonLess(tableRowData, cellRemainingAmount, cellRemainingPrice){
		return createButton("-", cellRemainingAmount, cellRemainingPrice, tableRowData,
			function(){ return parseInt(cellRemainingAmount.innerText, 10) - 1; },
			function(newValue){ return newValue >= 0; }
		);
	}
	
	function createEmptyFrontCell(){
		var cell = document.createElement("td");
		cell.classList.add("emptyCell");
		return cell;
	}
	
	function forceRedraw(){
		document.querySelector("#shoplist").style.display = "none";
		document.querySelector("#shoplist").style.display = "block";
	}
	
	publicApi.createTableRow = function(tableRowData){
		console.log(tableRowData);
		var row = document.createElement("tr");
		row.appendChild(createEmptyFrontCell());
		row.appendChild(createNameCell(tableRowData));
		row.appendChild(createIconCell(tableRowData));
		row.appendChild(createPriceCell(tableRowData));
		row.appendChild(createAvailableAmountCell(tableRowData));
		var cellRemainingAmount = createRemainingAmountCell(tableRowData);
		var cellRemainingPrice = createRemainingPriceCell(tableRowData);
		row.appendChild(createButtonLess(tableRowData, cellRemainingAmount, cellRemainingPrice));
		row.appendChild(cellRemainingAmount);
		row.appendChild(createButtonMore(tableRowData, cellRemainingAmount, cellRemainingPrice));
		row.appendChild(cellRemainingPrice);
		
		row.classList.add(tableRowData.rarity, getClassNameFriendlyString(tableRowData.name));
		
		document.querySelector("#shoplist").appendChild(row);
	};

	publicApi.initializePage = function(jsonSrc){
		publicApi.importTableItems(jsonSrc, function(data){
			console.log(data);
			publicApi.setPageTitle(data.title, true);
			publicApi.setFavIcon(data.favIconSrc, true);
			publicApi.setHeaderTitle(data.title);
			publicApi.setEventDuration(data.eventDuration);
			publicApi.setBuyoutPrice(publicApi.calculateBuyoutPrice(data.shop));
			publicApi.setHeaderImg(data.headerImg.src);
		
			for(var i = 0; i < data.shop.length; i++){
				publicApi.createTableRow(data.shop[i]);
			}
			initializePersistedStorage(data.shop, data.storageKey);
			
			document.querySelector("#export").addEventListener("click", function(){
				publicApi.exportAsJson(dict, data.jsonExportFileName + " (" + new Date().getTime() + ").json");
			}, false);
			
			document.querySelector("#import").addEventListener("change", publicApi.importJson);
		});
	}
	
	publicApi.setPageTitle = function(title, includeParent){
		document.title = title;
		if(includeParent) parent.document.title = title;
	}
	
	publicApi.calculateBuyoutPrice = function(tableRows){
		var buyoutPrice = 0;
		for(var i = 0; i < tableRows.length; i++){
			console.log(buyoutPrice);
			buyoutPrice += (tableRows[i].price * tableRows[i].availableAmount);
		}
		return buyoutPrice;
	}
	
	//#region persisted storage
	var storageKey = "defaultStorage";
	var dict = {};
	
	publicApi.setStorageKey = function(_storageKey){
		storageKey = _storageKey;
	}
	
	function updatePersistedStorage(key, value){
		dict[key] = value;
		if(typeof localStorage !== 'undefined'){
			localStorage.setItem(storageKey, JSON.stringify(dict));
		}
	}
	
	function initializePersistedStorage(shopItems, _storageKey){
		storageKey = _storageKey;
		dict = loadPersistedStorage();
		applyPersistedValues();
	}
	
	function loadPersistedStorage(){
		if (typeof localStorage !== 'undefined'){
			var string = localStorage.getItem(storageKey);
			if(string != null){
				return JSON.parse(string);
			}
			return {};
		}
	}
	
	function applyPersistedValues(){
		Object.keys(dict).forEach(function(key) {
			var className = getClassNameFriendlyString(key);
			var select = document.querySelector("." + className + " > .cellRemaining");
			if(select !== null){
				select.innerText = dict[key];
			}
		});
	}
	
	function getClassNameFriendlyString(string){
		return string.replace(/[ \(\)]+/g, "_");
	}
	//#endregion
	
	return publicApi;
})();