(() => {
  var CACHE = "orchestrion-tracker";
  var staticAssets = [
    "/",
    "/orchestrion/index.hmtl",
    "/orchestrion/updates.hmtl",
    "/script.js",
    "/styles.css",
    "/icons/*.png"
  ];

  self.addEventListener("install", function () {
    event.waitUntil(caches.open(CACHE).then(cache => { cache.addAll(staticAssets); }));
  });

  self.addEventListener("activate", event => {

  });

  self.addEventListener("fetch", event => {
    event.respondWith(fromCache(event.request));

    event.waitUntil(update(event.request).then(refresh));

    /*event.respondWith(
      caches.match(event.request, { ignoreSearch: true })
        .then(response => { return response || fetch(event.request); })
        .catch(reason => { console.log(reason); })
    );*/
  });

  function fromCache(request){
    return caches.open(CACHE).then(cache => { return cache.match(request); });
  }

  function update(request){
    return caches.open(CACHE).then(cache => {
       return fetch(request).then(response => {
          return cache.put(request, response.clone()).then(_ => response)
        })
      });
  }

  function refresh(response){
    return self.clients.matchAll().then(clients => {
      clients.forEach(client => {
        var message = {type: "refresh", url: "response.url"};
        client.postMessage(JSON.stringify(message));
      })
    });
  }
})();