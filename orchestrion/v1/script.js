var hideObtainedActive = false;
var canUseStorage = false;

function a(){
	canUseStorage = typeof(Storage) !== "undefined";
	if(canUseStorage){
		loadLocalStorageValues();
	}else{
		console.log("web storage not available");
	}
	
	setExpandableClickListeners();
	setRollClickListeners();
	setHideObtainedListener();
}

function setExpandableClickListeners(){
	var exp = document.getElementsByClassName("expandable");
	var i;
	for (i = 0; i < exp.length; i++) {
		exp[i].addEventListener("click", function(){
			this.classList.toggle("expanded");	
			var list = this.nextElementSibling;
			if (list.style.display === "block") {
				list.style.display = "none";
			} else {
				list.style.display = "block";
			}
		});
		getCategoryCount(exp[i]);
	} 
}

function getCategoryCount(category){
	var inCategory = category.nextElementSibling.getElementsByClassName("roll").length;
	var obtained = category.nextElementSibling.getElementsByClassName("obtained").length;
	var text = category.innerText;
	if(text.indexOf(" ") > 0){
		text = text.substring(0, text.indexOf(" "));
	}
	category.innerText = text + " ( " + obtained + " / " + inCategory + " )"
	if(obtained == inCategory){
		category.classList.add("categoryComplete");
	}else{
		category.classList.remove("categoryComplete");
	}
}

function setRollClickListeners(){
	var rolls = document.getElementsByClassName("roll");
	var i;
	for(i = 0; i < rolls.length; i++){
		rolls[i].addEventListener("click", function(){
			this.classList.toggle("obtained");
			if(canUseStorage){
				var val = this.classList.contains("obtained") ? "1" : "0";
				localStorage.setItem(this.innerHTML, val)
			}
			if(hideObtainedActive && this.classList.contains("obtained")){
				this.style.display = "none";
			}
			getCategoryCount(this.parentElement.previousElementSibling);
		});
	}
}

function loadLocalStorageValues(){
	var b = document.getElementById("hideObtained");
	var hide = localStorage.getItem("hideObtained");
	hideObtainedActive = hide == "1";
	setHideObtainedButtonStyles();
	
	var p = document.getElementsByTagName("p")
	var text = [];
	for(var i = 0; i < p.length; i++){
		var entry = p[i].innerHTML;
		storageValue = localStorage.getItem(entry);
		if(storageValue == "1"){
			p[i].classList.toggle("obtained");
			if(hideObtainedActive) p[i].style.display = "none";
		}
	}
}

function setHideObtainedListener(){
	var b = document.getElementById("hideObtained");
	b.addEventListener("click", function(){
		hideObtainedActive = !hideObtainedActive;
		if(canUseStorage){
			var val = hideObtainedActive ? "1" : "0";
			localStorage.setItem("hideObtained", val);
		}
		toggleRollsHidden();
		setHideObtainedButtonStyles();
	});
}

function setHideObtainedButtonStyles(){
	var b = document.getElementById("hideObtained");
	b.innerText = (hideObtainedActive ? "[✔]" : "[✘]") + " Hide obtained";
	b.style.color = hideObtainedActive ? "#809FFF" : "#9F3855";
}

function toggleRollsHidden(){
	var rolls = document.getElementsByClassName("roll");
	for(i = 0; i < rolls.length; i++){
		if(hideObtainedActive && rolls[i].classList.contains("obtained")){
			rolls[i].style.display = "none";
		}else{
			rolls[i].style.display = "block";
		}
	}
}