var hideObtainedActive = false;
var canUseStorage = false;

//local storage related values
var LOCAL_STORAGE_KEY = "OrchestrionTrackerStorage";
var HIDE_OBTAINED_KEY = "Setting_HideObtained";
var HIDE_FOOTER_KEY = "Setting_HideFooter";
var dict = {};

function a(){
	canUseStorage = typeof(Storage) !== "undefined";
	if(canUseStorage){
		loadLocalStorageValues();
	}else{
		console.log("web storage not available");
	}
	
	setContentHeight();
	setResizeListener();
	setExpandableClickListeners();
	setRollClickListeners();
	setHideObtainedListener();
	setExportJsonListener();
	setMigrationListener();
	initImport();
	setMenuListener();
	setFooterListeners();
}

function setExpandableClickListeners(){
	var exp = document.getElementsByClassName("expandable");
	var i;
	for (i = 0; i < exp.length; i++) {
		exp[i].addEventListener("click", function(){
			this.classList.toggle("expanded");	
			var list = this.nextElementSibling;
			if (list.style.display === "block") {
				list.style.display = "none";
			} else {
				list.style.display = "block";
			}
		});
		getCategoryCount(exp[i]);
	} 
}

function getCategoryCount(category){
	var inCategory = category.nextElementSibling.getElementsByClassName("roll").length;
	var obtained = category.nextElementSibling.getElementsByClassName("obtained").length;
	var text = category.innerText;
	if(text.indexOf(" ") > 0){
		text = text.substring(0, text.indexOf(" "));
	}
	category.innerText = text + " ( " + obtained + " / " + inCategory + " )";
	if(obtained == inCategory){
		category.classList.add("categoryComplete");
	}else{
		category.classList.remove("categoryComplete");
	}
}

function setRollClickListeners(){
	var rolls = document.getElementsByClassName("roll");
	var i;
	for(i = 0; i < rolls.length; i++){
		rolls[i].addEventListener("click", function(){
			this.classList.toggle("obtained");
			if(canUseStorage){
				var val = this.classList.contains("obtained") ? "1" : "0";
				dict[this.innerText] = val;
				saveToLocalStorage();
			}
			if(hideObtainedActive && this.classList.contains("obtained")){
				this.style.display = "none";
			}
			getCategoryCount(this.parentElement.previousElementSibling);
		});
	}
}

function loadLocalStorageValues(){
	if(!canUseStorage) return;
	dict = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
	if(dict === null) dict = {};
	
	var b = document.getElementById("hideObtained");
	hideObtainedActive = dict[HIDE_OBTAINED_KEY] == "1";
	setHideObtainedButtonStyles();
	
	var p = document.getElementsByTagName("p")
	var text = [];
	for(var i = 0; i < p.length; i++){
		var entry = p[i].innerHTML;
		storageValue = dict[entry];
		
		if(storageValue == "1"){
			p[i].classList.add("obtained");
			if(hideObtainedActive) p[i].style.display = "none";
		}else{
			p[i].classList.remove("obtained");
		}
	}
	
	if(dict[HIDE_FOOTER_KEY] == "1"){
		document.getElementById("footer").style.display = "none";
		document.getElementById("footerCollapsed").style.display = "block";
	}
}

function setHideObtainedListener(){
	var b = document.getElementById("hideObtained");
	b.addEventListener("click", hideObtained);
}

function hideObtained(){
	hideObtainedActive = !hideObtainedActive;
	if(canUseStorage){
		var val = hideObtainedActive ? "1" : "0";
		dict[HIDE_OBTAINED_KEY] = val;
		saveToLocalStorage();
	}
	toggleRollsHidden();
	setHideObtainedButtonStyles();
}

function setHideObtainedButtonStyles(){
	var b = document.getElementById("hideObtained");
	b.innerText = (hideObtainedActive ? "[✔]" : "[✘]") + " Hide obtained";
	b.style.color = hideObtainedActive ? "#809FFF" : "#9F3855";
}

function toggleRollsHidden(){
	var rolls = document.getElementsByClassName("roll");
	for(i = 0; i < rolls.length; i++){
		if(hideObtainedActive && rolls[i].classList.contains("obtained")){
			rolls[i].style.display = "none";
		}else{
			rolls[i].style.display = "block";
		}
	}
}

function setExportJsonListener(){
	document.getElementById("sm_exportProgress").addEventListener("click", function(){
		exportProgressJson();
	});
}

function saveToLocalStorage(){
	if(canUseStorage) localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(dict));
}

function exportProgressJson(){
	var a = document.createElement("a");
	var file = new Blob([JSON.stringify(dict, null, "\t")], {type: "application/json"});
	a.href = URL.createObjectURL(file);
	a.download = "OrchestrionTrackerExport_" + new Date().getTime() + ".json";
	document.body.appendChild(a);
	a.click();
	document.body.removeChild(a);
	toggleMenu();
}

function setMigrationListener(){
	var migrate = document.getElementById("sm_migrate");
	migrate.addEventListener("click", function(){
		for(var i = 0; i < localStorage.length; i++){
			var key = localStorage.key(i);
			var value = localStorage.getItem(localStorage.key(i));
			if(key == LOCAL_STORAGE_KEY){
				console.log("skipping " + LOCAL_STORAGE_KEY);
				continue;
			}
			if(key == "hideObtained") key = HIDE_OBTAINED_KEY;
			dict[key] = value;
			console.log("migrating key:value == " + key + ":" + value);
		}
			saveToLocalStorage();
			loadLocalStorageValues();
			toggleMenu();
			location.reload();
	});
}

function initImport(){
	document.getElementById("importProgress").addEventListener("change", handleFileSelect);
}

function handleFileSelect(event){
	toggleMenu();
	var file = event.target.files[0];
	console.log("Importing from file: " + file);
	if(file.type === "application/json"){
		var reader = new FileReader();
		reader.onload = (function(f) {
			return function(e) {
			  importProgressJson(e.target.result);
			};
		})(file);
		reader.readAsText(file);
	}
}
function importProgressJson(importedText){
	dict = JSON.parse(importedText);
	saveToLocalStorage();
}

function setMenuListener(){
	document.getElementById("menuToggle").addEventListener("click", toggleMenu);
	document.getElementById("sm_closeMenu").addEventListener("click", toggleMenu);
	document.getElementById("sm_toggleFooter").addEventListener("click", function(){
		toggleFooterVisibility()
		toggleMenu();
	});
}

function toggleMenu(){
	var menu = document.getElementById("sideMenu");
	if(menu.style.display == "flex"){
		menu.style.display = "none";
	}else{
		menu.style.display= "flex";
	}
}

function setContentHeight(){
	var margin = 0;
	if(document.getElementById("footer").style.display === "none") margin = 15;
	var totalHeight = document.documentElement.clientHeight;
	var contentHeight =  Math.floor(totalHeight - document.getElementById(getVisibleFooterId()).clientHeight - document.getElementById("controls").clientHeight - margin) + "px";
	document.getElementById("content").style.height = contentHeight;
}

function setResizeListener(){
	window.addEventListener("resize", setContentHeight);
}

function toggleFooterVisibility(){
	var visible = dict[HIDE_FOOTER_KEY] == "0";
	var footer = document.getElementById("footer");
	var footerCollapsed = document.getElementById("footerCollapsed");
	footer.style.display = visible ? "none" : "block";
	footerCollapsed.style.display = visible ? "block" : "none";
	dict[HIDE_FOOTER_KEY] = visible ? "1" : "0";
	saveToLocalStorage();
	setContentHeight();
}

function setFooterListeners(){
	document.getElementById("closeFooter").addEventListener("click", function(){
		toggleFooterVisibility();
	});
	document.querySelector("#footerCollapsed > a").addEventListener("click", function(){
		toggleFooterVisibility();
	});
}

function getVisibleFooterId(){
	var visible = dict[HIDE_FOOTER_KEY] == "0";
	return visible ? "footer" : "footerCollapsed";
}