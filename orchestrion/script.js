var hideObtainedActive = false;
var canUseStorage = false;
var onlyShowNew = false;

//local storage related values
var LOCAL_STORAGE_KEY = "OrchestrionTrackerStorage";
var HIDE_OBTAINED_KEY = "Setting_HideObtained";
var HIDE_FOOTER_KEY = "Setting_HideFooter";
var SHOW_NEW_ONLY_KEY = "Setting_ShowNewOnly";
var dict = {};

function a(){
	canUseStorage = typeof(Storage) !== "undefined";
	if(canUseStorage){
		loadLocalStorageValues();
	}else{
		console.log("web storage not available");
	}

	if('serviceWorker' in navigator){
		navigator.serviceWorker.register('serviceworker.js')
			.then(registration => console.log('ServiceWorker registered'))
			.catch(err => 'SeruceWorker registration failed');

		navigator.serviceWorker.onmessage = msg => {
			var message = JSON.parse(msg.data);
			if(message.type === "refresh"){
				//do refresh
			}
		};
	}else{
		console.log("ServiceWorker is not supported!");
	}
	
	setContentHeight();
	setResizeListener();
	setExpandableClickListeners();
	setRollClickListeners();
	setHideObtainedListener();
	setExportJsonListener();
	initImport();
	setMenuListener();
	setFooterListeners();
	setOnlyShowNewListener();
}

function setExpandableClickListeners(){
	var exp = document.getElementsByClassName("expandable");
	var i;
	for (i = 0; i < exp.length; i++) {
		exp[i].addEventListener("click", function(){
			this.classList.toggle("expanded");	
			var list = this.nextElementSibling;
			if (list.style.display === "block") {
				list.style.display = "none";
			} else {
				list.style.display = "block";
			}
		});
		getCategoryCount(exp[i]);
	} 
}

function getCategoryCount(category){
	var inCategory = category.nextElementSibling.querySelectorAll("p").length;
	var obtained = category.nextElementSibling.querySelectorAll("p.obtained").length;
	var text = category.innerText;
	if(text.indexOf(" ") > 0){
		text = text.substring(0, text.indexOf(" "));
	}
	category.innerText = text + " ( " + obtained + " / " + inCategory + " )";
	if(obtained == inCategory){
		category.classList.add("categoryComplete");
	}else{
		category.classList.remove("categoryComplete");
	}
}

function setRollClickListeners(){
	var rolls = document.body.querySelectorAll("div.roll-list > p");
	var i;
	for(i = 0; i < rolls.length; i++){
		rolls[i].addEventListener("click", function(){
			this.classList.toggle("obtained");
			if(canUseStorage){
				var val = this.classList.contains("obtained") ? "1" : "0";
				dict[this.innerText] = val;
				saveToLocalStorage();
			}
			if(hideObtainedActive && this.classList.contains("obtained")){
				this.style.display = "none";
			}
			getCategoryCount(this.parentElement.previousElementSibling);
		});
	}
}

function loadLocalStorageValues(){
	if(!canUseStorage) return;
	dict = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
	if(dict === null) dict = {};
	
	hideObtainedActive = dict[HIDE_OBTAINED_KEY] == "1";
	onlyShowNew = dict[SHOW_NEW_ONLY_KEY] == "1";
	setRollVisibilities();
	updateHideObtained();
	updateOnlyShowNew();
	
	var p = document.getElementsByTagName("p")
	for(var i = 0; i < p.length; i++){
		var entry = p[i].innerHTML;
		storageValue = dict[entry];
		
		if(storageValue == "1"){
			p[i].classList.add("obtained");
			if(hideObtainedActive) p[i].style.display = "none";
		}else{
			p[i].classList.remove("obtained");
		}
	}
	
	if(dict[HIDE_FOOTER_KEY] == "1"){
		document.getElementById("footer").style.display = "none";
		document.getElementById("footerCollapsed").style.display = "block";
	}
}

function setExportJsonListener(){
	document.getElementById("sm_exportProgress").addEventListener("click", function(){
		exportProgressJson();
	});
}

function saveToLocalStorage(){
	if(canUseStorage) localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(dict));
}

function exportProgressJson(){
	var a = document.createElement("a");
	var file = new Blob([JSON.stringify(dict, null, "\t")], {type: "application/json"});
	a.href = URL.createObjectURL(file);
	a.download = "OrchestrionTrackerExport_" + new Date().getTime() + ".json";
	document.body.appendChild(a);
	a.click();
	document.body.removeChild(a);
	toggleMenu();
}

function initImport(){
	document.getElementById("importProgress").addEventListener("change", handleFileSelect);
}

function handleFileSelect(event){
	toggleMenu();
	var file = event.target.files[0];
	console.log("Importing from file: " + file);
	if(file.type === "application/json"){
		var reader = new FileReader();
		reader.onload = (function(f) {
			return function(e) {
			  importProgressJson(e.target.result);
			};
		})(file);
		reader.readAsText(file);
	}
}
function importProgressJson(importedText){
	dict = JSON.parse(importedText);
	saveToLocalStorage();
}

function setMenuListener(){
	document.getElementById("menuToggle").addEventListener("click", toggleMenu);
	document.getElementById("sm_closeMenu").addEventListener("click", toggleMenu);
	document.getElementById("sm_toggleFooter").addEventListener("click", function(){
		toggleFooterVisibility()
		toggleMenu();
	});
}

function toggleMenu(){
	var menu = document.getElementById("sideMenu");
	if(menu.style.display == "flex"){
		menu.style.display = "none";
	}else{
		menu.style.display= "flex";
	}
}

function setContentHeight(){
	var margin = 0;
	if(document.getElementById("footer").style.display === "none") margin = 15;
	var totalHeight = document.documentElement.clientHeight;
	var contentHeight =  Math.floor(totalHeight - document.getElementById(getVisibleFooterId()).clientHeight - document.getElementById("controls").clientHeight - margin) + "px";
	document.getElementById("content").style.height = contentHeight;
}

function setResizeListener(){
	window.addEventListener("resize", setContentHeight);
}

function toggleFooterVisibility(){
	var visible = dict[HIDE_FOOTER_KEY] == "0";
	var footer = document.getElementById("footer");
	var footerCollapsed = document.getElementById("footerCollapsed");
	footer.style.display = visible ? "none" : "block";
	footerCollapsed.style.display = visible ? "block" : "none";
	dict[HIDE_FOOTER_KEY] = visible ? "1" : "0";
	saveToLocalStorage();
	setContentHeight();
}

function setFooterListeners(){
	document.getElementById("closeFooter").addEventListener("click", function(){
		toggleFooterVisibility();
	});
	document.querySelector("#footerCollapsed > a").addEventListener("click", function(){
		toggleFooterVisibility();
	});
}

function getVisibleFooterId(){
	var visible = dict[HIDE_FOOTER_KEY] == "0";
	return visible ? "footer" : "footerCollapsed";
}

function setHideObtainedListener(){
	document.querySelectorAll(".hideObtained").forEach(el => {
		el.addEventListener("click", hideObtained);
	});
}

function hideObtained(event){
	hideObtainedActive = !hideObtainedActive;
	if(canUseStorage){
		var val = hideObtainedActive ? "1" : "0";
		dict[HIDE_OBTAINED_KEY] = val;
		saveToLocalStorage();
	}
	
	setRollVisibilities();
	updateHideObtained();
}

function updateHideObtained(){
	document.querySelectorAll(".hideObtained").forEach(el => {
		setButtonStyle(el, hideObtainedActive, "Hide obtained");
	});
}

function setOnlyShowNewListener(){
	document.querySelectorAll(".onlyShowNew").forEach(el => {
		el.addEventListener("click", toggleNewScrolls);
	});
}

function toggleNewScrolls(){
	onlyShowNew = !onlyShowNew;
	if(canUseStorage){
		var value = onlyShowNew ? "1" : "0";
		dict[SHOW_NEW_ONLY_KEY] = value;
		saveToLocalStorage();
	}
	
	setRollVisibilities();
	updateOnlyShowNew();
}

function updateOnlyShowNew(){
	document.querySelectorAll(".onlyShowNew").forEach(el => {
		setButtonStyle(el, onlyShowNew, "Only show new");
	});
}


function setRollVisibilities(){
	var rolls = document.querySelectorAll("div.roll-list > p");
	for(i = 0; i < rolls.length; i++){
		setVisibility(rolls[i]);
	}
}

function setButtonStyle(button, condition, stringSuffix){
	var inlcudeCondition = button.tagName.toLowerCase() == "li";
	var conditionText = inlcudeCondition ? (condition ?  "[✔] " : "[✘] ") : "";
	button.innerText = conditionText  + stringSuffix;
	button.style.color = condition ? "#809FFF" : "#9F3855";
}


function setVisibility(element){
	var visibile = onlyShowNew ? element.classList.contains("new") : true;
	visible = visibile && (hideObtainedActive ? !element.classList.contains("obtained") : true);
	element.style.display = visible ? "block" : "none";
}
