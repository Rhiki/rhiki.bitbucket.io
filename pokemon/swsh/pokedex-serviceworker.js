(() => {

    const staticCache = "poxedex-cache-v4";
    const staticAssets = [
        "/",
        "pokedex.css",
        "pokedex-darkmode.css",
        "pokedex-smallscreen.css",
        "index.html",
        "exclusive-mons.html",
        "pokedex.js",
        "pokedex.json"
    ];

    self.addEventListener("install", (event) => {
        event.waitUntil(caches.open(staticCache).then(cache => cache.addAll(staticAssets)));
    });

    self.addEventListener("fetch", (event) => {
        event.respondWith(
            caches.open(staticCache).then((cache) => {
                return cache.match(event.request).then((response) => {
                    return response || fetch(event.request).then((response) => {
                        cache.put(event.request, response.clone());
                        return response;
                    })
                })
            })
        );
    });

    self.addEventListener("activate", (event) => {
        const cacheWhitelist = [staticCache];
        event.waitUntil(
            caches.keys().then(cacheNames => {
                return Promise.all(
                    cacheNames.map(cacheName => {
                        if (cacheWhitelist.indexOf(cacheName) === -1) {
                            return caches.delete(cacheName);
                        }
                    })
                );
            })
        );
    });
})();