(() => {
    window.addEventListener("DOMContentLoaded", () => {
        loadPokedex();
        if (readStorage("first_time", true)) {
            showAbout();
            writeStorage("first_time", false);
        }
        registerUI();
        registerServiceWorker();
    });

    /**
     * Add event listener for UI elements.
     */
    function registerUI() {
        document.querySelectorAll(".close_dialog").forEach((element) => element.addEventListener("click", () => hideDialogs()));
        document.querySelector("#open_about").addEventListener("click", () => showAbout());
        document.querySelector("#open_settings").addEventListener("click", () => showSettings());
        document.querySelector("#full_screen_bg_fade").addEventListener("click", (event) => { if (event.target.id == "full_screen_bg_fade") hideDialogs(); });
        registerFilters();
        registerSettings();
    }

    /**
     * Register events for filter selections.
     */
    function registerFilters() {
        document.querySelector("#filter_exclusive").addEventListener("change", () => executeFilters());
        document.querySelector("#filter_galarian_form").addEventListener("change", () => executeFilters());
        document.querySelector("#filter_type_1").addEventListener("change", () => executeFilters());
        document.querySelector("#filter_type_2").addEventListener("change", () => executeFilters());
        document.querySelector("#filter_gigantamax").addEventListener("change", () => executeFilters());
        document.querySelector("#filter_reset").addEventListener("click", () => resetFilters());
    }

    /**
     * Register events for setting inputs and buttons. Also set the correct value in the UI from localStorage.
     */
    function registerSettings() {
        // Inputs
        let smallIcons = document.querySelector("#setting_small_icons");
        smallIcons.checked = readStorage("small_icons", false);
        smallIcons.addEventListener("change", (event) => useSmallIcons(event.target.checked));

        let gradientBg = document.querySelector("#setting_gradient_bg");
        gradientBg.checked = readStorage("gradient_bg", false);
        gradientBg.addEventListener("change", (event) => useGradientBg(event.target.checked));

        let gradientOpacity = document.querySelector("#setting_gradient_opacity");
        gradientOpacity.value = readStorage("gradient_bg_opacity", 0.3);
        gradientOpacity.addEventListener("change", () => updateGradientOpacity());

        let showTypes = document.querySelector("#setting_show_types");
        showTypes.checked = readStorage("show_type_icons", true);
        showTypes.addEventListener("click", (event) => showTypeIcons(event.target.checked));

        let showVariations = document.querySelector("#setting_show_variation_checkboxes");
        showVariations.checked = readStorage("show_variation_boxes", true);
        showVariations.addEventListener("click", (event) => showVariationBoxes(event.target.checked));

        // Buttons
        document.querySelector("#setting_cache_images").addEventListener("click", () => cacheAllImages());
        document.querySelector("#setting_clear_cache").addEventListener("click", () => clearCaches());
        document.querySelector("#setting_refresh").addEventListener("click", () => window.location.reload());
    }

    function registerServiceWorker() {
        if ("serviceWorker" in navigator) {
            navigator.serviceWorker.register("./pokedex-serviceworker.js")
                .then(registration => console.log("Servicerworker registered!"))
                .catch(error => console.log("Serviceworker registration failed!"));
        }
    }

    const LOCAL_STORAGE_KEY = "SWSH_POKEDEX_STORAGE";
    var storage = {};

    /**
     * Read a value from the localStorage if possible
     * @param {string} key - the key to retreive
     * @param {*} defaultValue the  default value to return if no value for the key was found
     * @returns the value associated with the key; or the value passed in defaultValue if the key was not found
     */
    function readStorage(key, defaultValue) {
        if (!storage.hasOwnProperty("init")) {
            if (typeof (Storage) !== "undefined") {
                storage = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
                if (storage === null) {
                    storage = {}; // first time
                }
            }
            storage["init"] = true;
        }
        return storage.hasOwnProperty(key) ? storage[key] : defaultValue;
    }

    /**
     * Write a key/value pair to the storage variable and saves it to localStorage if possible
     * @param {string} key - the key to write to
     * @param {any} value - the value to write
     */
    function writeStorage(key, value) {
        storage[key] = value;
        if (typeof (Storage) !== "undefined") {
            localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(storage));
        }
    }


    var completeDex = null; // Gets set in loadPokedex();
    const ExclusivityFilter = Object.freeze({ "all": "all", "sword": "sword", "shield": "shield" });
    const GalarianFormFilter = Object.freeze({ "all": "all", "galarian_form_only": "galarian_form_only" });
    const GigantamaxFilter = Object.freeze({ "all": "all", "gigantamax_only": "gigantamax_only" });
    const TypeFilter = Object.freeze({
        "All": "All",
        "Bug": "Bug",
        "Dark": "Dark",
        "Dragon": "Dragon",
        "Electric": "Electric",
        "Fairy": "Fairy",
        "Fighting": "Fighting",
        "Fire": "Fire",
        "Flying": "Flying",
        "Ghost": "Ghost",
        "Grass": "Grass",
        "Ground": "Ground",
        "Ice": "Ice",
        "Normal": "Normal",
        "Poison": "Poison",
        "Psychic": "Psychic",
        "Rock": "Rock",
        "Steel": "Steel",
        "Water": "Water"
    });

    /**
     * Class representing a pokemon.
     */
    class Pokemon {
        /**
         * @param {string} galarId - ID in the Galarian Pokedex
         * @param {string} nationalId - ID in the National Pokedex
         * @param {string} name - Name of the pokemon
         * @param {Array<String>} types - Types of the pokemon
         * @param {Array<String>} catchHint - Hints on how to catch a pokemon
         * @param {boolean} hasGalarianForm - Whether or not the Pokemon has a Galarian form
         * @param {boolean} swordExclusive - Whether or not the Pokemon can only be caught in Sword
         * @param {boolean} shieldExclusive - Whether or not the Pokemon can only be caught in Shield
         * @param {Gigantamax} gigantamax - Gigantamax information; null if no Gigantamax form exists
         */
        constructor(galarId, nationalId, name, types, hasGalarianForm, swordExclusive,
            shieldExclusive, gigantamax, catchHint) {
            this.galarId = galarId;
            this.nationalId = nationalId;
            this.name = name;
            this.types = types;
            this.evolChain = evolChain;
            this.hasGalarianForm = hasGalarianForm;
            this.swordExclusive = swordExclusive;
            this.shieldExclusive = shieldExclusive;
            this.gigantamax = gigantamax;
            this.catchHint = catchHint;
            this.gigantamaxExclusive = gigantamaxExclusive;
        }

        /**
         * Get a string representation of the pokemon
         * @param {boolean} includeNr - include the ID of the pokemon
         * @param {boolean} useNational - use the national dex ID instead of the Galar dex ID
         */
        toString(includeNr, useNational) {
            if (includeNr) {
                return `#${useNational ? this.nationalId : this.galarId} - ${this.name}`;
            }
            return `${this.name}`;
        }

        /**
         * Returns a sanitized version of the name to be used in DOM IDs
         * @returns {string}
         */
        idSafeName() {
            return this.name.replace(/\W/gi, "");
        }

        /**
         * Get the image id. Return value is either "<nationalId>" or "<nationalId>-g", if this Pokemon is a Galarian form.
         * @returns {string} 
         */
        imageId() {
            return `${this.nationalId}${this.hasGalarianForm ? "-g" : ""}`;
        }

        /**
         * Whether or not this Pokemon matches the exclusivity filter
         * @param {ExclusivityFilter} filter - the type of exclusivity to compare against
         * @returns {boolean} true if matches; false otherwise
         */
        matchesExclusivityFilter(filter) {
            if (filter == ExclusivityFilter.sword) {
                return this.swordExclusive;
            } else if (filter == ExclusivityFilter.shield) {
                return this.shieldExclusive;
            }
            return true;
        }

        /**
         * Whether or not this Pokemon matches the garlian form filter
         * @param {GalarianFormFilter} filter - the type of exclusivity to compare against
         * @returns {boolean} true if matches; false otherwise
         */
        matchesGarlianFormFilter(filter) {
            if (filter == GalarianFormFilter.galarian_form_only) {
                return this.hasGalarianForm;
            }
            return true;
        }

        /**
         * Whether or not this Pokemon matches the type filter
         * @param {TypeFilter} filter - the type to compare against
         * @returns {boolean} true if matches; false otherwise
         */
        matchesTypeFilter(filter) {
            if (filter == TypeFilter.All) {
                return true;
            }
            return this.types.includes(filter);
        }

        /**
         * Whether or not this Pokemon matches the gigantamax filter
         * @param {GigantamaxFilter} filter
         */
        matchesGigantamaxFilter(filter) {
            if (filter == GigantamaxFilter.gigantamax_only) {
                return this.gigantamax;
            }
            return true;
        }

        /**
         * Convenience method for calling @method matchesTypeFilter with two filters.
         * Order is irrelevant.
         * @param {TypeFilter} filter1 - the type to compare against
         * @param {TypeFilter} filter2 - the type to compare against
         * @returns {boolean} true if matches; false otherwise
         */
        matchesTwoTypeFilter(filter1, filter2) {
            return this.matchesTypeFilter(filter1) && this.matchesTypeFilter(filter2);
        }
    }

    /**
     * Class containing Gigantamax information.
     */
    class Gigantamax {
        /**
         * 
         * @param {boolean} available - whether or not this Pokemon has a Gigantamax form
         * @param {String} versionExclusivity - "Sword"/"Shield" if this Gigantamax form is only
         * available in one of the games, empty string or null if it's available in both games.
         * @param {String} otherExclusivity - any other exclusivity if applicable (for example: timed events,
         * requiring Let's Go saves, mystery gift)
         */
        constructor(available, versionExclusivity, otherExclusivity) {
            this.available = available;
            this.versionExclusivity = versionExclusivity;
            this.otherExclusivity = otherExclusivity;
        }
    }

    /**
     * Load the Galar pokedex from a remote JSON file and create the UI
     * in @
     */
    function loadPokedex() {
        let dexSource = "./pokedex.json";
        fetch(dexSource)
            .then(response => response.json())
            .then(json => {
                completeDex = json.sort((a, b) => {
                    return a.galarId - b.galarId;
                });
                initPokedexUI(completeDex)
            });
    }

    const colors = {
        "Fire": "rgba(247, 82, 49, $opacity)",
        "Bug": "rgba(173, 189, 33, $opacity)",
        "Dark": "rgba(115, 90, 74, $opacity)",
        "Dragon": "rgba(123, 99, 231, $opacity)",
        "Electric": "rgba(255, 198, 49, $opacity)",
        "Fairy": "rgba(247, 181, 247, $opacity)",
        "Fighting": "rgba(165, 82, 57, $opacity)",
        "Fire": "rgba(247, 82, 49, $opacity)",
        "Flying": "rgba(156, 173, 247, $opacity)",
        "Ghost": "rgba(99, 99, 181, $opacity)",
        "Grass": "rgba(123, 206, 82, $opacity)",
        "Ground": "rgba(214, 181, 90, $opacity)",
        "Ice": "rgba(90, 206, 231, $opacity)",
        "Normal": "rgba(173, 165, 148, $opacity)",
        "Poison": "rgba(181, 90, 165, $opacity)",
        "Psychic": "rgba(255, 115, 165, $opacity)",
        "Rock": "rgba(189, 165, 90, $opacity)",
        "Steel": "rgba(173, 173, 198, $opacity)",
        "Water": "rgba(57, 156, 255, $opacity)"
    }

    /**
     * Generate the UI for each pokemon.
     * @param {Array<Pokemon>} pokedex
     */
    function initPokedexUI(pokedex) {
        let dexContainer = document.querySelector("#dex_container");
        let useSmallIcons = readStorage("small_icons", false);
        let useGradientBg = readStorage("gradient_bg", false);
        let showTypeIcons = readStorage("show_type_icons", true);
        pokedex.forEach(pokemon => {
            Object.setPrototypeOf(pokemon, Pokemon.prototype);
            let container = document.createElement("div");
            let linkWrapper = document.createElement("div");
            linkWrapper.classList.add("link_wrapper");
            container.classList.add("pokemon_container");
            container.id = pokemon.idSafeName();

            let link = document.createElement("a");
            link.href = `https://www.serebii.net/pokedex-swsh/${pokemon.name.toLowerCase().replace(" ", "")}/`
            link.target = "_blank";
            link.rel = "noopener";
            if (pokemon.swordExclusive) {
                container.classList.add("sword");
            } else if (pokemon.shieldExclusive) {
                container.classList.add("shield");
            }
            let image = document.createElement("img");
            image.src = `./image/pokemon/${pokemon.imageId()}.webp`;
            image.alt = pokemon.name;

            if (useGradientBg) {
                image.style.background = generateGradientBg(pokemon, image);
            }
            if (useSmallIcons) {
                image.classList.add("small");
            }
            image.classList.add(...pokemon.types);
            link.appendChild(image);

            let span = document.createElement("span");
            span.innerText = pokemon.toString(true);
            link.appendChild(span);
            linkWrapper.appendChild(link);

            let options = document.createElement("div");
            options.classList.add("pokemon_additional");

            addToggle(options, "✨", `shiny_toggle_${pokemon.idSafeName()}`, () => updatePokemonIcon(pokemon));
            if (pokemon.gigantamax) {
                addToggle(options, "⏫", `gigantamax_toggle_${pokemon.idSafeName()}`, () => updatePokemonIcon(pokemon));
            }

            let infoString = generateInfo(pokemon);
            if (infoString) {
                let infobox = document.createElement("a");
                infobox.classList.add("infobox");
                infobox.innerText = " 🛈 ";
                infobox.title = infoString;
                options.appendChild(infobox);
            }

            let typeIconClasses = showTypeIcons ? ["type_icon"] : ["type_icon", "hidden"];
            pokemon.types.forEach(type => {
                let typeImage = document.createElement("img");
                typeImage.src = `./image/type/${type.toLowerCase()}.gif`;
                typeImage.alt = type;
                typeImage.classList.add(...typeIconClasses);
                options.appendChild(typeImage);
            });
            // Commented out for now, would need to add this all to the JSON file.
            // And I think it might be a bit too much info for the layout.
            //options.appendChild(generateHint(pokemon));

            container.appendChild(linkWrapper);
            container.appendChild(options);
            dexContainer.appendChild(container);
        });
        executeFilters();
    }

    /**
     * Generate a string that contains the various information about the pokemon
     * @param {Pokemon} pokemon - the pokemon who's information string to generate
     * @returns {String} compiled information
     */
    function generateInfo(pokemon) {
        var info = "";
        console.log(pokemon.gigantamax);
        if (pokemon.gigantamax) {
            if (pokemon.gigantamax.versionExclusivity) {
                info += `\nThe Gigantamax Max Raid battle for ${pokemon.name} is only available in ${pokemon.gigantamax.versionExclusivity}.`;
            }
            if (pokemon.gigantamax.otherExclusivity) {
                info += `\nGigantamax: ${pokemon.gigantamax.otherExclusivity}`;
            }
        }
        return info.trim();
    }


    /**
     * Generate a span that contains the hint information
     * @param {Pokemon} pokemon - the pokemon who's hint to generate
     * @returns {Element} a <span>-Element containing the hint
     */
    function generateHint(pokemon) {
        let span = document.createElement("span");
        if (pokemon.catchHint == null || pokemon.catchHint.length <= 0) return span;
        let pokemonRegex = /^(Evolve )(\w+)(.*)$/; // [2] = Pokemon name
        for (var i = 0; i < pokemon.catchHint.length; i++) {
            let hint = pokemon.catchHint[i];
            let match = hint.match(pokemonRegex);
            if (match !== null) {
                let link = document.createElement("a");
                link.classList.add("scroll_to_pokemon");
                link.addEventListener("click", () => {
                    let target = document.querySelector(`#${match[2]}`);
                    target.scrollIntoView({ behavior: 'smooth', block: 'nearest', inline: 'start' })
                    target.style.transition = '0.8s';
                    target.style.backgroundColor = "rgba(128, 159, 255, 0.3)";
                    setTimeout(() => target.style.backgroundColor = "", 1000);
                    setTimeout(() => target.style.transition = "", 1800);
                });
                link.innerText = match[2];
                span.append(match[1]);
                span.appendChild(link);
                span.append(match[3]);
            } else {
                span.append(hint);
            }
            if (i < pokemon.catchHint.length - 1) {
                span.append(", ");
            }
        }
        return span;
    }

    /**
     * Update an icon
     * @param {Pokemon} pokemon - the pokemon, which' image to update
     */
    function updatePokemonIcon(pokemon) {
        let gigantamax = pokemon.gigantamax ? document.querySelector(`#gigantamax_toggle_${pokemon.idSafeName()}`).checked : false;
        let shiny = document.querySelector(`#shiny_toggle_${pokemon.idSafeName()}`).checked;
        let image = document.querySelector(`#${pokemon.idSafeName()} img`);
        image.src = getImageUrl(pokemon, shiny, gigantamax);
    }

    function getImageUrl(pokemon, shiny, gigantamax) {
        if (gigantamax && shiny) {
            return `./image/pokemon-shiny/gigantamax/${pokemon.nationalId}.webp`;
        } else if (gigantamax) {
            return `./image/pokemon/gigantamax/${pokemon.nationalId}.webp`
        } else if (shiny) {
            return `./image/pokemon-shiny/${pokemon.imageId()}.webp`
        } else {
            return `./image/pokemon/${pokemon.imageId()}.webp`;
        }
    }

    /**
     * 
     * @param {Element} target - the element to append to
     * @param {string} hint - the label text
     * @param {stirng} checkBoxId - the id for the checkbox
     * @param {function} onchange - onchange callback
     */
    function addToggle(target, hint, checkboxId, onchange) {
        let showBoxes = readStorage("show_variation_boxes", true);
        let classes = showBoxes ? ["variation_toggle"] : ["variation_toggle", "hidden"];
        let checkbox = document.createElement("input");
        checkbox.id = checkboxId;
        checkbox.type = "checkbox";
        checkbox.classList.add(...classes);
        checkbox.addEventListener("change", event => onchange(event.target.checked));

        let label = document.createElement("label");
        label.innerText = hint;
        label.htmlFor = checkbox.id;
        label.classList.add(...classes);
        target.appendChild(label);
        target.appendChild(checkbox);
    }

    /**
     * Resets all applied filters
     */
    function resetFilters() {
        document.querySelectorAll("#filters > select").forEach(element => {
            element.selectedIndex = 0;
        });
        executeFilters();
    }

    /**
     * Reset visibility of all pokemon.
     */
    function resetVisibility() {
        document.querySelectorAll(".pokemon_container").forEach(element => {
            element.classList.remove("filtered");
        });
    }

    /**
     * Execute set filters.
     */
    function executeFilters() {
        let hide = [];
        let exclusivity = document.querySelector("#filter_exclusive").value;
        let galarianForm = document.querySelector("#filter_galarian_form").value;
        let type1 = document.querySelector("#filter_type_1").value;
        let type2 = document.querySelector("#filter_type_2").value;
        let gigantamax = document.querySelector("#filter_gigantamax").value;
        completeDex.forEach(pokemon => {
            if (!pokemon.matchesExclusivityFilter(exclusivity)) {
                hide.push(pokemon);
            } else if (!pokemon.matchesGarlianFormFilter(galarianForm)) {
                hide.push(pokemon);
            } else if (!pokemon.matchesTwoTypeFilter(type1, type2)) {
                hide.push(pokemon);
            } else if (!pokemon.matchesGigantamaxFilter(gigantamax)) {
                hide.push(pokemon);
            }
        });

        resetVisibility();
        hide.forEach(toHide => {
            document.querySelector(`#${toHide.idSafeName()}`).classList.add("filtered");
        });
    }

    /**
     * Set usage of small icons
     * @param {boolean} value - true to use small icons; false to use original size icons
     */
    function useSmallIcons(value) {
        document.querySelectorAll("img").forEach(img => {
            img.classList.toggle("small", value);
        });
        writeStorage("small_icon", value);
    }

    /**
     * Set usage of type-based gradient background
     * @param {boolean} value - true to use gradients; false to disable gradients
     */
    function useGradientBg(value) {
        let opacity = readStorage("gradient_bg_opacity", 0.3);
        completeDex.forEach(pokemon => {
            let target = document.querySelector(`#${pokemon.idSafeName()}`);
            let background = value ? generateGradientBg(pokemon, opacity) : "";
            target.style.background = background;
        });
        writeStorage("gradient_bg", value);
    }

    function updateGradientOpacity() {
        let value = document.querySelector("#setting_gradient_opacity").value;
        writeStorage("gradient_bg_opacity", value);
        useGradientBg(document.querySelector("#setting_gradient_bg").checked);
    }

    /**
     * Generate a pokemon's type-based gradient background
     * @param {Pokemon} pokemon - the pokemon from which to take type information
     * @param {number} opacity - the opacity of the gradient, between (inlcuding) 0.0 and (including) 1.0
     * @returns {string} the string to apply to the background property
     */
    function generateGradientBg(pokemon, opacity) {
        if (pokemon.types.length > 1) {
            // Two-type pokemon
            return `radial-gradient(at 40% 50%, ${colors[pokemon.types[0]].replace("$opacity", opacity)}, transparent 50%), radial-gradient(at 60% 50%, ${colors[pokemon.types[1]].replace("$opacity", opacity)}, transparent 50%)`;
        } else {
            // One-type pokemon
            return `radial-gradient(${colors[pokemon.types[0]].replace("$opacity", opacity)}, transparent 60%)`;
        }
    }

    function hideDialogs() {
        document.querySelector("#full_screen_bg_fade").style.display = "none";
        document.querySelector("#about_content").style.display = "none";
        document.querySelector("#settings_content").style.display = "none";
    }

    function showAbout() {
        document.querySelector("#full_screen_bg_fade").style.display = "block";
        document.querySelector("#about_content").style.display = "block";
    }

    function showSettings() {
        document.querySelector("#full_screen_bg_fade").style.display = "block";
        document.querySelector("#settings_content").style.display = "block";
    }

    function cacheAllImages() {
        completeDex.forEach(pokemon => {
            fetch(getImageUrl(pokemon, true, false));
            if (pokemon.hasGalarianForm) {
                fetch(getImageUrl(pokemon, false, true));
                fetch(getImageUrl(pokemon, true, true));
            }
        });
    }

    function clearCaches() {
        if (window.caches) { // Safeguard in case cache doesn't exit (ie page served over HTTP in Chrome)
            caches.keys().then(c => c.forEach(cache => caches.delete(cache)))
        }
    }

    function showTypeIcons(value) {
        document.querySelectorAll("img.type_icon").forEach(element => element.classList.toggle("hidden"), !value);
        writeStorage("show_type_icons", value);
    }

    function showVariationBoxes(value) {
        document.querySelectorAll(".variation_toggle").forEach(element => element.classList.toggle("hidden", !value));
        writeStorage("show_variation_boxes", value);
    }
})();